package app.in.flyk.utilities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import app.in.flyk.R;
import com.qtos.ApiClient;

import java.net.InetAddress;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by infiny on 23/1/18.
 */

public class CommonUtils {



    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

        public static void hideLoading(ProgressDialog mProgressDialog) {
        Log.i("mProgressDialog"," ===== > "+mProgressDialog );
            if  ( mProgressDialog != null ) {
                mProgressDialog.setCanceledOnTouchOutside(true);
                if(mProgressDialog.isShowing()){
                    Log.i("mProgressDialog"," ==isShowing=== > "+mProgressDialog );
                    mProgressDialog.dismiss();
                    Log.i("mProgressDialog"," ==isShowing=== > "+mProgressDialog.isShowing() );
                }

            }
        }

    // methods to validate the email format
    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static void checkInternetConnetion(final Context context){
        if(!isNetworkAvailable(context)) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder.setTitle("Network");
            builder.setMessage("Check your internet connection & try again");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    context.startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                }
            });
            builder.setIcon(R.drawable.ic_icon_transaction_pending);
            AlertDialog dialog = builder.show();
            dialog.show();
        }

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

/*
        {
            try {
                InetAddress.getByName("http://52.66.115.197").isReachable(3000); //Replace with your name
                return true;
            } catch (Exception e) {
                return false;
            }*/

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void checkLocationServices(final Context mContext) {
        AlertDialog.Builder alertDialog;
        alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("Network");
        // Setting Dialog Message
        alertDialog.setMessage("To continue, let your device turn on location, which uses location service");
        alertDialog.setCancelable(false);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                checkLocationServices(mContext);
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


}
