package com.example.infiny.boozr;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CrashReportActivity extends AppCompatActivity {

//  Handling Uncaught Exceptions
// HandleAppCrash.deploy(this,CrashReportActivity.class);
    private TextView _report;
    private Button _btnclose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_report);
        ActionBar ab = getSupportActionBar();
        if(ab!=null) {
            ab.hide();
        }
        Intent intent  = getIntent();
        _report = (TextView) findViewById(R.id.report);
        _report.setText(intent.getStringExtra("stackTrace"));
        _btnclose = (Button) findViewById(R.id.btnclose);

        _btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }
}
